package org.poncho;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SorterMainTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private String[] args;
    private String expectedOutput;
    public SorterMainTest(String[] args, String expectedOutput) {
        this.args = args;
        this.expectedOutput = expectedOutput;
    }
    // Arrange
    @Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() throws Exception {
        System.setOut(originalOut);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"8", "-20", "30"}, "Sorted array:\r\n-20 8 30"},
                {new String[]{"10", "20", "abc"}, "Argument 3 is not a valid integer: abc"},
                {new String[]{}, "No command line arguments recived"},
                {new String[]{"10"}, "Sorted array:\r\n10"},
                {new String[]{"10", "3", "4", "8", "1", "6", "2", "7", "5", "9"}, "Sorted array:\r\n1 2 3 4 5 6 7 8 9 10"},
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"}, "Too many command line arguments. Maximum is 10."}
        });
    }

    @Test
    public void testArguments() {
        // Act
        SorterMain.main(args);

        // Assert
        String printedOutput = outContent.toString().trim();
        assertEquals(expectedOutput, printedOutput);
    }

}