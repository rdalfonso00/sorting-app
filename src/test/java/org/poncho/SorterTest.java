package org.poncho;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SorterTest {

    private final Sorter sorting = new Sorter();
    private int[] inputArray;
    private int[] expectedArray;

    public SorterTest(int[] inputArray, int[] expectedArray) {
        this.inputArray = inputArray;
        this.expectedArray = expectedArray;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new int[]{}, new int[]{} },
                { new int[]{5}, new int[]{5} },
                { new int[]{1, 6, 9, 20, 55, 101}, new int[]{1, 6, 9, 20, 55, 101} },
                { new int[]{101, -100, 55, 45, 99, 0, 1}, new int[]{-100, 0, 1, 45, 55, 99, 101} }
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortNullInput(){
        int[] array = null;
        sorting.sort(array);
    }

    @Test
    public void testSort() {
        sorting.sort(inputArray);
        assertArrayEquals(expectedArray, inputArray);
    }
}