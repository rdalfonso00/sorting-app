package org.poncho;
public class SorterMain {
    public static void main(String [] args){
        if(args.length == 0){
            System.out.println("No command line arguments recived");
            return;
        }

        if (args.length > 10) {
            System.out.println("Too many command line arguments. Maximum is 10.");
            return;
        }

        int[] numbersList = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbersList[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.out.println("Argument " + (i + 1) + " is not a valid integer: " + args[i]);
                return;
            }
        }

        Sorter sorter = new Sorter();
        sorter.sort(numbersList);
        System.out.println("Sorted array:");
        for (int num : numbersList) {
            System.out.print(num + " ");
        }
    }
}
